/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/06 11:44:25 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/21 19:04:56 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_H
# define SO_LONG_H

# include "../libft/libft.h"
# include "../mlx_linux/mlx.h"
# include <unistd.h>
# include <fcntl.h>

typedef struct s_img
{
	void	*img;
	char	*addr;
	int		h;
	int		w;
	int		bits_per_pixel;
	int		endian;
	int		line_len;
}			t_img;

typedef struct s_win
{
	t_img	img;
	void	*mlx;
	void	*win;
	int		width;
	int		height;
	char	**map;
	int		count_p;
	int		count_e;
	int		count_c;
	int		co_e_x;
	int		co_e_y;
	int		co_p_x;
	int		co_p_y;
	int		num_move;
	int		frame;
	t_img	player_w;
	t_img	player_a;
	t_img	player_s;
	t_img	player_d;
	t_img	wall;
	t_img	floor;
	t_img	collect;
	t_img	exit;
	t_img	exit_disable;	
}			t_win;

void	flood_fill(char **map, int x, int y, int *i);
int		way_check(t_win *window);
int		check_width(t_win *window);
void	read_map(char *file, t_win *window);
int		count_items(t_win *window);
int		check_walls(t_win *window);
void	clean_map(t_win *window);
int		sizeof_map(t_win *window);
void	coords(t_win *window, int i, int j, int type);
void	check_map(t_win *window);
void	close_game(t_win *window);
void	game_start(t_win *window);
void	img_start(t_win *window);
t_img	create_img(char *path, t_win *window);
void	img_start(t_win *window);
void	clean_img(t_win *window);
void	pixel_put(t_win *win, int x, int y, int color);
void	texture_show(t_img *item, t_win *window, int x, int y);
int		controls(int key, t_win	*window);
void	w_a_s_d(t_win *win, char direction, int dx, int dy);
void	refresh_img(t_win *win, int x, int y, char dir);
int		update_animations(t_win *window);
void	update_frames(t_win *win, int he, int wi);

#endif