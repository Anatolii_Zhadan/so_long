/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long_bonus.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/06 11:44:25 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/23 18:41:07 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_BONUS_H
# define SO_LONG_BONUS_H

# include "../libft/libft.h"
# include "../mlx_linux/mlx.h"
# include <unistd.h>
# include <fcntl.h>

typedef struct s_img
{
	void	*img;
	char	*addr;
	int		h;
	int		w;
	int		bits_per_pixel;
	int		endian;
	int		line_len;
}			t_img;

typedef struct s_win
{
	t_img	img;
	void	*mlx;
	void	*win;
	int		width;
	int		height;
	char	**map;
	int		count_p;
	int		count_e;
	int		count_c;
	int		co_e_x;
	int		co_e_y;
	int		co_p_x;
	int		co_p_y;
	int		co_k_x;
	int		co_k_y;
	int		num_move;
	int		frame;
	char	dir;
	char	k_dir;
	t_img	player_w[5];
	t_img	player_a[5];
	t_img	player_s[5];
	t_img	player_d[5];
	t_img	wall;
	t_img	floor;
	t_img	collect[5];
	t_img	exit[6];
	t_img	enemy_d[5];
	t_img	enemy_a[5];
}			t_win;

//checkers_bonus.c
void	flood_fill(char **map, int x, int y, int *i);
int		way_check(t_win *window);
int		check_width(t_win *window);
void	read_map(char *file, t_win *window);
int		count_items(t_win *window);
//control_bonus.c
int		controls(int key, t_win *window);
void	update_position(t_win *win, int dx, int dy);
int		direction_w_a_s_d(char dir, int *dx, int *dy);
void	w_a_s_d(t_win *win, char direction, int dx, int dy);
void	refresh_img(t_win *win, int x, int y, char dir);
//enemy_bonus.c
void	enemy_update_position(t_win *win, int dx, int dy);
void	enemy_refresh_img(t_win *win, int x, int y, char dir);
void	enemy_w_a_s_d(t_win *win, char direction, int dx, int dy);
void	enemy_compare(t_win *win, int flag);
int		update_animations(t_win *window);
//img_bonus.c
void	show_img_checker(t_win *win, int he, int wi);
void	show_img(t_win *win, int he, int wi);
void	img_start(t_win *window);
void	img_start_player(t_win *window);
void	clean_img(t_win *window);
//map_bonus.c
int		check_walls(t_win *window);
void	pixel_put(t_win *win, int x, int y, int color);
void	update_frames_checker(t_win *win, int he, int wi);
void	update_frames(t_win *win, int he, int wi);
t_img	create_img(char *path, t_win *window);
//so_long_bonus.c
void	clean_map(t_win *window);
int		sizeof_map(t_win *window);
void	coords(t_win *window, int i, int j, int type);
void	check_map(t_win *window);
//window_bonus.c
void	close_game(t_win *window);
void	game_start(t_win *window);
void	texture_show(t_img *item, t_win *window, int x, int y);
void	write_on_screen(t_win *win);
void	clean_img_player(t_win *window);

#endif