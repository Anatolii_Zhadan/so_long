NAME = so_long
BONUS = so_long_bonus

SRCS = files/so_long.c files/window.c files/img.c \
		 files/checkers.c files/map.c files/control.c
OBJS = $(SRCS:.c=.o)

B_SRCS = files_bonus/so_long_bonus.c files_bonus/window_bonus.c files_bonus/img_bonus.c \
		 files_bonus/checkers_bonus.c files_bonus/map_bonus.c files_bonus/control_bonus.c \
		 files_bonus/enemy_bonus.c
B_OBJS = $(B_SRCS:.c=.o)

CFLAGS = -Wall -Wextra -Werror -g -Ih_files/
MLXFLAGS = -lXext -lX11 -lm -lz
LIBFT = libft/libft.a
MLX = mlx_linux/libmlx_Linux.a


all: $(NAME) bonus

$(NAME): $(OBJS)
		@make -C mlx_linux/ --no-print
		@make -C libft/ --no-print
		@cc $(CFLAGS) $(OBJS) $(LIBFT) $(MLX) $(MLXFLAGS) -o $(NAME) 

%.o: %.c
	@$(CC) -g -Wall -Wextra -Werror -I/usr/include -Imlx_linux -O3 -c $< -o $@

clean:
	@rm -f $(OBJS) $(B_OBJS)

fclean: clean
	@rm -f $(NAME) $(BONUS)

bonus: $(B_OBJS)
		@make -C mlx_linux/ --no-print
		@make -C libft/ --no-print
		@cc $(CFLAGS) $(B_OBJS) $(LIBFT) $(MLX) $(MLXFLAGS) -o $(BONUS) 

re: fclean all

.PHONY: all, fclean, re
