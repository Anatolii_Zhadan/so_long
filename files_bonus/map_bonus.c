/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_bonus.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 22:39:27 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/23 19:17:28 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long_bonus.h"

int	check_walls(t_win *window)
{
	int	i;

	i = 0;
	while (i < window->height)
	{
		if (window->map[i][0] != '1' || window->map[i][window->width
			- 1] != '1')
			return (0);
		else
			i++;
	}
	i = 0;
	while (i < window->width)
	{
		if (window->map[0][i] != '1' || window->map[window->height
			- 1][i] != '1')
			return (0);
		else
			i++;
	}
	return (1);
}

void	pixel_put(t_win *win, int x, int y, int color)
{
	char	*dst;

	dst = win->img.addr + (y * win->img.line_len + x * \
	(win->img.bits_per_pixel / 8));
	*(unsigned int *)dst = color;
}

void	update_frames_checker(t_win *win, int he, int wi)
{
	if (win->map[he][wi] == 'P')
	{
		texture_show(&win->floor, win, wi, he);
		if (win->dir == 'd')
			texture_show(&win->player_d[win->frame], win, wi, he);
		else if (win->dir == 's')
			texture_show(&win->player_s[win->frame], win, wi, he);
		else if (win->dir == 'a')
			texture_show(&win->player_a[win->frame], win, wi, he);
		else if (win->dir == 'w')
			texture_show(&win->player_w[win->frame], win, wi, he);
	}
	if (win->map[he][wi] == 'K')
	{
		texture_show(&win->floor, win, wi, he);
		if (win->k_dir == 'd')
			texture_show(&win->enemy_d[win->frame], win, wi, he);
		else if (win->k_dir == 's')
			texture_show(&win->enemy_d[win->frame], win, wi, he);
		else if (win->k_dir == 'a')
			texture_show(&win->enemy_a[win->frame], win, wi, he);
		else if (win->k_dir == 'w')
			texture_show(&win->enemy_a[win->frame], win, wi, he);
	}
}

void	update_frames(t_win *win, int he, int wi)
{
	while (win->map[++he])
	{
		wi = 0;
		while (win->map[he][wi])
		{
			if (win->map[he][wi] == 'E' && win->count_c == 0)
			{
				texture_show(&win->floor, win, wi, he);
				texture_show(&win->exit[win->frame], win, wi, he);
			}
			if (win->map[he][wi] == 'C')
			{
				texture_show(&win->floor, win, wi, he);
				texture_show(&win->collect[win->frame], win, wi, he);
			}
			update_frames_checker(win, he, wi);
			wi++;
			write_on_screen(win);
		}
	}
	mlx_put_image_to_window(win->mlx, win->win, win->img.img, 0, 0);
}

t_img	create_img(char *path, t_win *window)
{
	t_img	img;

	img.img = mlx_xpm_file_to_image(window->mlx, path, &img.w, &img.h);
	img.addr = mlx_get_data_addr(img.img, &img.bits_per_pixel, &img.line_len, \
	&img.endian);
	return (img);
}
