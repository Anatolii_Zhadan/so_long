/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   enemy_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/23 03:09:47 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/23 19:17:47 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long_bonus.h"

void	enemy_update_position(t_win *win, int dx, int dy)
{
	win->map[win->co_k_y][win->co_k_x] = '0';
	win->map[win->co_k_y + dy][win->co_k_x + dx] = 'K';
	win->co_k_x += dx;
	win->co_k_y += dy;
}

void	enemy_refresh_img(t_win *win, int x, int y, char dir)
{
	texture_show(&win->floor, win, x, y);
	if (dir == 'w')
	{
		texture_show(&win->floor, win, x, y - 1);
		texture_show(&win->enemy_d[win->frame], win, x, y - 1);
	}
	else if (dir == 'a')
	{
		texture_show(&win->floor, win, x - 1, y);
		texture_show(&win->enemy_a[win->frame], win, x - 1, y);
	}
	else if (dir == 's')
	{
		texture_show(&win->floor, win, x, y + 1);
		texture_show(&win->enemy_a[win->frame], win, x, y + 1);
	}
	else if (dir == 'd')
	{
		texture_show(&win->floor, win, x + 1, y);
		texture_show(&win->enemy_d[win->frame], win, x + 1, y);
	}
	mlx_put_image_to_window(win->mlx, win->win, win->img.img, 0, 0);
}


void	enemy_w_a_s_d(t_win *win, char direction, int dx, int dy)
{
	int	x;
	int	y;

	x = win->co_k_x;
	y = win->co_k_y;
	if (direction == 'w')
		dy = -1;
	else if (direction == 's')
		dy = 1;
	else if (direction == 'a')
		dx = -1;
	else if (direction == 'd')
		dx = 1;
	else
		return ;
	if (win->map[y + dy][x + dx] == '1' || win->map[y + dy][x + dx] == 'E' || \
	win->map[y + dy][x + dx] == 'C')
		return ;
	if (win->map[y + dy][x + dx] == 'P')
		close_game(win);
	win->k_dir = direction;
	enemy_refresh_img(win, x, y, direction);
	enemy_update_position(win, dx, dy);
}

void	enemy_compare(t_win *win, int flag)
{
	if (flag == 0)
	{
		if (win->co_p_x > win->co_k_x)
			enemy_w_a_s_d(win, 'd', 0, 0);
		else if (win->co_p_x < win->co_k_x)
			enemy_w_a_s_d(win, 'a', 0, 0);
	}
	if (flag == 1)
	{
		if (win->co_p_y > win->co_k_y)
			enemy_w_a_s_d(win, 's', 0, 0);
		else if (win->co_p_y < win->co_k_y)
			enemy_w_a_s_d(win, 'w', 0, 0);
	}
}

int	update_animations(t_win *window)
{
	static int	i;

	if (++i % 6000 == 0)
	{
		if (i % 12000 == 0)
			enemy_compare(window, 0);
		else if (i % 18000 == 0)
			enemy_compare(window, 1);
		window->frame++;
		if (window->frame > 4)
		{
			window->frame = 0;
			i = 0;
		}
		update_frames(window, -1, 0);
	}
	write_on_screen(window);
	return (1);
}
