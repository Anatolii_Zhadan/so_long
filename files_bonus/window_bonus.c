/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window_bonus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/08 01:39:42 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/23 21:01:31 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long_bonus.h"

void	close_game(t_win *window)
{
	clean_map(window);
	clean_img(window);
	if (window->win)
		mlx_destroy_window(window->mlx, window->win);
	mlx_destroy_display(window->mlx);
	free(window->mlx);
	exit (0);
}

void	game_start(t_win *window)
{
	window->mlx = mlx_init();
	window->win = mlx_new_window(window->mlx, window->width * 64 \
			, window->height * 64, "Say hello to the camera!");
}

void	texture_show(t_img *item, t_win *window, int x, int y)
{
	int				i;
	int				j;
	unsigned int	color;

	j = 0;
	while (j < item->h)
	{
		i = 0;
		while (i < item->w)
		{
			color = *(unsigned int *)(item->addr + j * item->line_len + i * (\
			item->bits_per_pixel / 8));
			if (color != 4278190080)
				pixel_put(window, x * 64 + i, y * 64 + j, color);
			i++;
		}
		j++;
	}
}

void	write_on_screen(t_win *win)
{
	char	*number;
	char	*out_put;

	number = ft_itoa(win->num_move);
	out_put = ft_strjoin("Move number: ", number);
	mlx_string_put(win->mlx, win->win, 100, 100, 0x853DC6, out_put);
	free(number);
	free(out_put);
}

void	clean_img_player(t_win *window)
{
	mlx_destroy_image(window->mlx, window->player_w[0].img);
	mlx_destroy_image(window->mlx, window->player_w[1].img);
	mlx_destroy_image(window->mlx, window->player_w[2].img);
	mlx_destroy_image(window->mlx, window->player_w[3].img);
	mlx_destroy_image(window->mlx, window->player_w[4].img);
	mlx_destroy_image(window->mlx, window->player_a[0].img);
	mlx_destroy_image(window->mlx, window->player_a[1].img);
	mlx_destroy_image(window->mlx, window->player_a[2].img);
	mlx_destroy_image(window->mlx, window->player_a[3].img);
	mlx_destroy_image(window->mlx, window->player_a[4].img);
	mlx_destroy_image(window->mlx, window->player_s[0].img);
	mlx_destroy_image(window->mlx, window->player_s[1].img);
	mlx_destroy_image(window->mlx, window->player_s[2].img);
	mlx_destroy_image(window->mlx, window->player_s[3].img);
	mlx_destroy_image(window->mlx, window->player_s[4].img);
	mlx_destroy_image(window->mlx, window->player_d[0].img);
	mlx_destroy_image(window->mlx, window->player_d[1].img);
	mlx_destroy_image(window->mlx, window->player_d[2].img);
	mlx_destroy_image(window->mlx, window->player_d[3].img);
	mlx_destroy_image(window->mlx, window->player_d[4].img);
	mlx_destroy_image(window->mlx, window->enemy_d[0].img);
	mlx_destroy_image(window->mlx, window->enemy_d[1].img);
	mlx_destroy_image(window->mlx, window->enemy_d[2].img);
	mlx_destroy_image(window->mlx, window->enemy_d[3].img);
	mlx_destroy_image(window->mlx, window->enemy_d[4].img);
}
