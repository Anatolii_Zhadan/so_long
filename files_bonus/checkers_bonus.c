/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkers_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 22:37:24 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/23 19:17:54 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long_bonus.h"

void	flood_fill(char **map, int x, int y, int *i)
{
	if (map[y][x] == '1' || map[y][x] == 'F')
		return ;
	if (map[y][x] == 'C' || map[y][x] == 'E')
		*i += 1;
	map[y][x] = 'F';
	flood_fill(map, x + 1, y, i);
	flood_fill(map, x - 1, y, i);
	flood_fill(map, x, y + 1, i);
	flood_fill(map, x, y - 1, i);
}

int	way_check(t_win *window)
{
	int		counter;
	char	**map_tmp;
	int		i;

	counter = 0;
	i = -1;
	map_tmp = (char **)malloc(sizeof(char *) * (window->height + 1));
	if (!map_tmp)
		return (0);
	while (++i < window->height)
		map_tmp[i] = ft_strdup(window->map[i]);
	flood_fill(map_tmp, window->co_p_x, window->co_p_y, &counter);
	i = -1;
	while (++i < window->height)
		free(map_tmp[i]);
	free(map_tmp);

	return (counter == (window->count_e + window->count_c));
}

int	check_width(t_win *window)
{
	int	i;
	int	j;
	int	c;

	i = 0;
	while (window->map[++i])
	{
		c = 0;
		j = -1;
		while (window->map[i][++j])
			c++;
		if (window->width != c)
			return (0);
	}
	return (1);
}

void	read_map(char *file, t_win *window)
{
	char	*tmp;
	char	*tmp_result;
	char	*result;
	int		fd;

	fd = open(file, O_RDONLY);
	result = ft_strdup("");
	while (fd)
	{
		tmp = get_next_line(fd);
		if (!tmp)
			break ;
		tmp_result = ft_strjoin(result, tmp);
		free(result);
		result = tmp_result;
		window->height++;
		free(tmp);
	}
	window->map = ft_split(result, '\n');
	free(result);
	if (window->map[0] == 0)
		clean_map(window);
	window->width = ft_strlen(window->map[0]);
	close(fd);
}


int	count_items(t_win *window)
{
	int	i;
	int	j;

	i = 0;
	while (window->map[i])
	{
		j = 0;
		while (j < window->width)
		{
			if (window->map[i][j] == 'C')
				window->count_c++;
			else if (window->map[i][j] == 'E')
				coords(window, i, j, 0);
			else if (window->map[i][j] == 'P')
				coords(window, i, j, 1);
			else if (window->map[i][j] == 'K')
				coords(window, i, j, 2);
			else if (window->map[i][j] != '1' && window->map[i][j] != '0')
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}
