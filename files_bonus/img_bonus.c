/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img_bonus.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 00:15:38 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/23 19:17:33 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long_bonus.h"

void	show_img_checker(t_win *win, int he, int wi)
{
	if (win->map[he][wi] == '0' || win->map[he][wi] == 'C'\
			|| win->map[he][wi] == 'P' || win->map[he][wi] == 'E' \
			|| win->map[he][wi] == 'K')
	{
		texture_show(&win->floor, win, wi, he);
		if (win->map[he][wi] == 'C')
			texture_show(&win->collect[0], win, wi, he);
		else if (win->map[he][wi] == 'E')
			texture_show(&win->exit[5], win, wi, he);
		else if (win->map[he][wi] == 'P')
		{
			texture_show(&win->player_d[0], win, wi, he);
			win->dir = 'd';
		}
		else if (win->map[he][wi] == 'K')
		{
			texture_show(&win->enemy_d[0], win, wi, he);
			win->k_dir = 'd';
		}
	}
	else if (win->map[he][wi] == '1')
		texture_show(&win->wall, win, wi, he);
}

void	show_img(t_win *win, int he, int wi)
{
	while (win->map[++he])
	{
		wi = 0;
		while (win->map[he][wi])
		{
			show_img_checker(win, he, wi);
			wi++;
		}
	}
	mlx_put_image_to_window(win->mlx, win->win, win->img.img, 0, 0);
}

void	img_start(t_win *window)
{
	window->img.img = mlx_new_image(window->mlx, window->width * 64, \
	window->height * 64);
	window->img.addr = mlx_get_data_addr(window->img.img, \
	&(window->img.bits_per_pixel),
			&(window->img.line_len), &(window->img.endian));
	window->floor = create_img("textures/empty/00.xpm", window);
	window->wall = create_img("textures/wall/00.xpm", window);
	window->exit[0] = create_img("textures/exit/00.xpm", window);
	window->exit[1] = create_img("textures/exit/01.xpm", window);
	window->exit[2] = create_img("textures/exit/02.xpm", window);
	window->exit[3] = create_img("textures/exit/03.xpm", window);
	window->exit[4] = create_img("textures/exit/00.xpm", window);
	window->exit[5] = create_img("textures/exit/disable.xpm", window);
	window->collect[0] = create_img("textures/collectible/00.xpm", window);
	window->collect[1] = create_img("textures/collectible/01.xpm", window);
	window->collect[2] = create_img("textures/collectible/02.xpm", window);
	window->collect[3] = create_img("textures/collectible/03.xpm", window);
	window->collect[4] = create_img("textures/collectible/04.xpm", window);
	window->enemy_a[0] = create_img("textures/enemy/A_01.xpm", window);
	window->enemy_a[1] = create_img("textures/enemy/A_02.xpm", window);
	window->enemy_a[2] = create_img("textures/enemy/A_03.xpm", window);
	window->enemy_a[3] = create_img("textures/enemy/A_04.xpm", window);
	window->enemy_a[4] = create_img("textures/enemy/A_05.xpm", window);
	img_start_player(window);
	show_img(window, -1, 0);
}

void	img_start_player(t_win *window)
{
	window->player_w[0] = create_img("textures/players/W_1.xpm", window);
	window->player_w[1] = create_img("textures/players/W_2.xpm", window);
	window->player_w[2] = create_img("textures/players/W_3.xpm", window);
	window->player_w[3] = create_img("textures/players/W_4.xpm", window);
	window->player_w[4] = create_img("textures/players/W_5.xpm", window);
	window->player_a[0] = create_img("textures/players/A_1.xpm", window);
	window->player_a[1] = create_img("textures/players/A_2.xpm", window);
	window->player_a[2] = create_img("textures/players/A_3.xpm", window);
	window->player_a[3] = create_img("textures/players/A_4.xpm", window);
	window->player_a[4] = create_img("textures/players/A_5.xpm", window);
	window->player_s[0] = create_img("textures/players/S_1.xpm", window);
	window->player_s[1] = create_img("textures/players/S_2.xpm", window);
	window->player_s[2] = create_img("textures/players/S_3.xpm", window);
	window->player_s[3] = create_img("textures/players/S_4.xpm", window);
	window->player_s[4] = create_img("textures/players/S_5.xpm", window);
	window->player_d[0] = create_img("textures/players/D_1.xpm", window);
	window->player_d[1] = create_img("textures/players/D_2.xpm", window);
	window->player_d[2] = create_img("textures/players/D_3.xpm", window);
	window->player_d[3] = create_img("textures/players/D_4.xpm", window);
	window->player_d[4] = create_img("textures/players/D_5.xpm", window);
	window->enemy_d[0] = create_img("textures/enemy/D_01.xpm", window);
	window->enemy_d[1] = create_img("textures/enemy/D_02.xpm", window);
	window->enemy_d[2] = create_img("textures/enemy/D_03.xpm", window);
	window->enemy_d[3] = create_img("textures/enemy/D_04.xpm", window);
	window->enemy_d[4] = create_img("textures/enemy/D_05.xpm", window);
}

void	clean_img(t_win *window)
{
	mlx_destroy_image(window->mlx, window->floor.img);
	mlx_destroy_image(window->mlx, window->wall.img);
	mlx_destroy_image(window->mlx, window->exit[0].img);
	mlx_destroy_image(window->mlx, window->exit[1].img);
	mlx_destroy_image(window->mlx, window->exit[2].img);
	mlx_destroy_image(window->mlx, window->exit[3].img);
	mlx_destroy_image(window->mlx, window->exit[4].img);
	mlx_destroy_image(window->mlx, window->exit[5].img);
	mlx_destroy_image(window->mlx, window->collect[0].img);
	mlx_destroy_image(window->mlx, window->collect[1].img);
	mlx_destroy_image(window->mlx, window->collect[2].img);
	mlx_destroy_image(window->mlx, window->collect[3].img);
	mlx_destroy_image(window->mlx, window->collect[4].img);
	mlx_destroy_image(window->mlx, window->enemy_a[0].img);
	mlx_destroy_image(window->mlx, window->enemy_a[1].img);
	mlx_destroy_image(window->mlx, window->enemy_a[2].img);
	mlx_destroy_image(window->mlx, window->enemy_a[3].img);
	mlx_destroy_image(window->mlx, window->enemy_a[4].img);
	mlx_destroy_image(window->mlx, window->img.img);
	clean_img_player(window);
}
