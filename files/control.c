/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   control.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/20 00:41:08 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/21 19:02:51 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long.h"

int	controls(int key, t_win *window)
{
	if (key == 65307)
		close_game(window);
	if (key == 119)
		w_a_s_d(window, 'w', 0, 0);
	if (key == 115)
		w_a_s_d(window, 's', 0, 0);
	if (key == 97)
		w_a_s_d(window, 'a', 0, 0);
	if (key == 100)
		w_a_s_d(window, 'd', 0, 0);
	return (1);
}

void	update_position(t_win *win, int dx, int dy)
{
	win->map[win->co_p_y][win->co_p_x] = '0';
	win->map[win->co_p_y + dy][win->co_p_x + dx] = 'P';
	win->co_p_x += dx;
	win->co_p_y += dy;
	win->num_move++;
	ft_printf("Move number:%d\n", win->num_move);
}

void	w_a_s_d(t_win *win, char direction, int dx, int dy)
{
	int	x;
	int	y;

	x = win->co_p_x;
	y = win->co_p_y;
	if (direction == 'w')
		dy = -1;
	else if (direction == 's')
		dy = 1;
	else if (direction == 'a')
		dx = -1;
	else if (direction == 'd')
		dx = 1;
	else
		return ;
	if (win->map[y + dy][x + dx] == '1' || (win->map[y + dy][x + dx] == 'E'
			&& win->count_c != 0))
		return ;
	if (win->map[y + dy][x + dx] == 'E' && win->count_c == 0)
		close_game(win);
	else if (win->map[y + dy][x + dx] == 'C')
		win->count_c--;
	refresh_img(win, x, y, direction);
	update_position(win, dx, dy);
}

void	refresh_img(t_win *win, int x, int y, char dir)
{
	texture_show(&win->floor, win, x, y);
	if (dir == 'w')
	{
		texture_show(&win->floor, win, x, y - 1);
		texture_show(&win->player_w, win, x, y - 1);
	}
	else if (dir == 'a')
	{
		texture_show(&win->floor, win, x - 1, y);
		texture_show(&win->player_a, win, x - 1, y);
	}
	else if (dir == 's')
	{
		texture_show(&win->floor, win, x, y + 1);
		texture_show(&win->player_s, win, x, y + 1);
	}
	else if (dir == 'd')
	{
		texture_show(&win->floor, win, x + 1, y);
		texture_show(&win->player_d, win, x + 1, y);
	}
	if (win->count_c == 0)
		texture_show(&win->exit, win, win->co_e_x, win->co_e_y);
	mlx_put_image_to_window(win->mlx, win->win, win->img.img, 0, 0);
}
