/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   img.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/09 00:15:38 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/23 20:21:52 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long.h"

t_img	create_img(char *path, t_win *window)
{
	t_img	img;

	img.img = mlx_xpm_file_to_image(window->mlx, path, &img.w, &img.h);
	img.addr = mlx_get_data_addr(img.img, &img.bits_per_pixel, &img.line_len, \
	&img.endian);
	return (img);
}

void	texture_show(t_img *item, t_win *window, int x, int y)
{
	int				i;
	int				j;
	unsigned int	color;

	j = 0;
	while (j < item->h)
	{
		i = 0;
		while (i < item->w)
		{
			color = *(unsigned int *)(item->addr + j * item->line_len + i * (\
			item->bits_per_pixel / 8));
			if (color != 4278190080)
				pixel_put(window, x * 64 + i, y * 64 + j, color);
			i++;
		}
		j++;
	}
}

void	show_img(t_win *win, int he, int wi)
{
	while (win->map[++he])
	{
		wi = 0;
		while (win->map[he][wi])
		{
			if (win->map[he][wi] == '0' || win->map[he][wi] == 'C'\
			|| win->map[he][wi] == 'P' || win->map[he][wi] == 'E')
			{
				texture_show(&win->floor, win, wi, he);
				if (win->map[he][wi] == 'C')
					texture_show(&win->collect, win, wi, he);
				else if (win->map[he][wi] == 'E')
					texture_show(&win->exit_disable, win, wi, he);
				else if (win->map[he][wi] == 'P')
					texture_show(&win->player_d, win, wi, he);
			}
			else if (win->map[he][wi] == '1')
				texture_show(&win->wall, win, wi, he);
			wi++;
		}
	}
	mlx_put_image_to_window(win->mlx, win->win, win->img.img, 0, 0);
}

void	img_start(t_win *window)
{
	window->img.img = mlx_new_image(window->mlx, window->width * 64, \
	window->height * 64);
	window->img.addr = mlx_get_data_addr(window->img.img, \
	&(window->img.bits_per_pixel),
			&(window->img.line_len), &(window->img.endian));
	window->floor = create_img("textures/empty/00.xpm", window);
	window->wall = create_img("textures/wall/00.xpm", window);
	window->player_w = create_img("textures/players/W_1.xpm", window);
	window->player_a = create_img("textures/players/A_1.xpm", window);
	window->player_s = create_img("textures/players/S_1.xpm", window);
	window->player_d = create_img("textures/players/D_2.xpm", window);
	window->exit = create_img("textures/exit/00.xpm", window);
	window->exit_disable = create_img("textures/exit/disable.xpm", window);
	window->collect = create_img("textures/collectible/00.xpm", window);
	show_img(window, -1, 0);
}

void	clean_img(t_win *window)
{
	mlx_destroy_image(window->mlx, window->floor.img);
	mlx_destroy_image(window->mlx, window->wall.img);
	mlx_destroy_image(window->mlx, window->player_w.img);
	mlx_destroy_image(window->mlx, window->player_a.img);
	mlx_destroy_image(window->mlx, window->player_s.img);
	mlx_destroy_image(window->mlx, window->player_d.img);
	mlx_destroy_image(window->mlx, window->exit.img);
	mlx_destroy_image(window->mlx, window->exit_disable.img);
	mlx_destroy_image(window->mlx, window->collect.img);
	mlx_destroy_image(window->mlx, window->img.img);
}
