/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/06 11:28:02 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/23 21:00:10 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long.h"

void	clean_map(t_win *window)
{
	int	i;

	i = -1;
	while (window->map[++i])
		free(window->map[i]);
	free(window->map);
}

int	sizeof_map(t_win *window)
{
	int	i;

	i = 0;
	while (window->map[++i])
		if ((int)ft_strlen(window->map[i]) != window->width)
			return (0);
	return (1);
}

void	coords(t_win *window, int i, int j, int type)
{
	if (type == 0)
	{
		window->co_e_x = j;
		window->co_e_y = i;
		window->count_e++;
	}
	else
	{
		window->co_p_x = j;
		window->co_p_y = i;
		window->count_p++;
	}
}

void	check_map(t_win *window)
{
	int	checker;

	checker = 0;
	ft_printf("height%d\nwidth%d\n", window->height, window->width);
	if (!check_width(window) || window->height == window->width)
		ft_printf("Error\nwrreturn ;ong form of map\n", checker++);
	else if (!count_items(window))
		ft_printf("Error\nwrong char\n", checker++);
	else if (window->count_c < 1)
		ft_printf("Error\nwrong number of collectibles\n", checker++);
	else if (window->count_e != 1)
		ft_printf("Error\nwrong number of exits\n", checker++);
	else if (window->count_p != 1)
		ft_printf("Error\nwrong number of players\n", checker++);
	else if (!check_walls(window))
		ft_printf("Error\ndon't have walls around map\n", checker++);
	else if (!way_check(window))
		ft_printf("Error\nPlayer don't have way to exit\n", checker++);
	if (checker == 1)
	{
		clean_map(window);
		exit (0);
	}
}

int	main(int argc, char **argv)
{
	char	*ext;
	t_win	window;

	ft_memset(&window, 0, sizeof(t_win));
	if (argc != 2)
	{
		ft_printf("Error\nwrong number of arguments!\n");
		return (0);
	}
	ext = ft_strrchr(argv[1], '.');
	if (!(ext && (ft_strncmp(ext, ".ber", 4) == 0)))
	{	
		ft_printf("Error\nWrong type of file of map\n");
		return (0);
	}
	read_map(argv[1], &window);
	check_map(&window);
	window.mlx = mlx_init();
	window.win = mlx_new_window(window.mlx, window.width * 64 \
			, window.height * 64, "Say hello to the camera!");
	img_start(&window);
	mlx_hook(window.win, 17, 0L, (void *)close_game, &window);
	mlx_key_hook(window.win, &controls, &window);
	mlx_loop(window.mlx);
	return (0);
}
