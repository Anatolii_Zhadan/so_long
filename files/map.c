/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42lisboa.com>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/06/16 22:39:27 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/21 19:03:49 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../h_files/so_long.h"

int	check_walls(t_win *window)
{
	int	i;

	i = 0;
	while (i < window->height)
	{
		if (window->map[i][0] != '1' || window->map[i][window->width
			- 1] != '1')
			return (0);
		else
			i++;
	}
	i = 0;
	while (i < window->width)
	{
		if (window->map[0][i] != '1' || window->map[window->height
			- 1][i] != '1')
			return (0);
		else
			i++;
	}
	return (1);
}

void	pixel_put(t_win *win, int x, int y, int color)
{
	char	*dst;

	dst = win->img.addr + (y * win->img.line_len + x * \
	(win->img.bits_per_pixel / 8));
	*(unsigned int *)dst = color;
}
